# pretty print
import pprint
# use HTTP handling requests
import requests
# parsing structured data
from bs4 import BeautifulSoup
# to print with a readable formated HTML
pp = pprint.PrettyPrinter(indent=4)

URL = 'https://www.monster.com/jobs/search/?q=Software-Developer&where=Australia'
page = requests.get(URL)

# print redeable HTML test
# pp.pprint(soup)

# create a Beautiful Soup object
soup = BeautifulSoup(page.content, 'html.parser')

#  find that specific ID identified from browser inspector
results = soup.find(id='ResultsContainer')

# Display results in a redeable way
print(results.prettify())
